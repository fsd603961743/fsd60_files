<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.dao.StudentDao,com.dto.Student"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border='2' align='center'>
<%@ include file="StudentHomePage.jsp" %>
      <%Student student= (Student)request.getAttribute("student"); %>
		<tr>
		<th> StudentId    </th>
		<th> StudentName  </th>
		<th> Course   </th>
		<th> Gender   </th>
		<th> Mobile</th>
		<th> Email-Id </th>
		<th> Password </th>
		</tr>

		<tr>
		<td><%= student.getStudentId()    %></td>
		<td><%= student.getStudentName()  %></td>
		<td><%= student.getCourse()   %></td>
		<td><%= student.getGender()   %></td>
		<td><%= student.getMobile()   %></td>
		<td><%= student.getEmailId()  %></td>
		<td><%= student.getPassword()  %></td>
		</tr>	

		</table>
</body>
</html>