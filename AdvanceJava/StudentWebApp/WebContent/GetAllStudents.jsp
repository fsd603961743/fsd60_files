<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="com.dao.StudentDao, java.util.List, com.dto.Student" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student List</title>
</head>
<body bgcolor="lightyellow" text="green">
    <% 
    StudentDao studentDao = new StudentDao();
    List<Student> studentList = studentDao.getAllStudents();
    %>
    
    <%@include file="StudentHomePage.jsp" %>

    <table border="2">
        <tr>
            <th>StudentId</th>
            <th>StudentName</th>
            <th>Course</th>
            <th>Gender</th>
            <th>Mobile</th>
            <th>Email-Id</th>
            <th>Password</th>
        </tr>
        
        <% for (Student student : studentList) { %>
        <tr>
            <td><%= student.getStudentId() %></td>
            <td><%= student.getStudentName() %></td>
            <td><%= student.getCourse() %></td>
            <td><%= student.getGender() %></td>
            <td><%= student.getMobile() %></td>
            <td><%= student.getEmailId() %></td>
            <td><%= student.getPassword() %></td>
        </tr>
        <% } %>
    </table>
</body>
</html>
