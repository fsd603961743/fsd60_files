package com.dto;

public class Student {

	private int studentId ;
	private String StudentName;
	private String course;
	private String gender;
	private String mobile;
	private String emailId;
	private String password;
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public Student(int studentId, String studentName, String course, String gender, String mobile, String emailId,
			String password) {
		super();
		this.studentId = studentId;
		StudentName = studentName;
		this.course = course;
		this.gender = gender;
		this.mobile = mobile;
		this.emailId = emailId;
		this.password = password;
	}
	public Student() {
		super();
	}
	
	public String getStudentName() {
		return StudentName;
	}
	public void setStudentName(String studentName) {
		StudentName = studentName;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", StudentName=" + StudentName + ", course=" + course + ", gender="
				+ gender + ", mobile=" + mobile + ", emailId=" + emailId + ", password=" + password + "]";
	}

}
