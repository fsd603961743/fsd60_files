package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDao;
import com.dto.Student;

/**
 * Servlet implementation class GetStudentById
 */
@WebServlet("/GetStudentById")
public class GetStudentById extends HttpServlet {
	
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	response.setContentType("text/html");
	PrintWriter out = response.getWriter();

	int empId = Integer.parseInt(request.getParameter("StudentId"));

	StudentDao studentDao = new StudentDao();
	Student student = studentDao.getStudentById(empId);

	out.print("<body bgcolor='lightyellow' text='green'>");	
		


	if (student != null) {
		request.setAttribute("student", student);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("GetStudentById.jsp");
		requestDispatcher.include(request, response);
	} else {
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("GetStudById.jsp");
		requestDispatcher.include(request, response);

		out.print("<h3 style='color:red;'>Unable to Fetch the Student Record</h3>");
	}

	out.print("</body>");

}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
