package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDao;
import com.dto.Student;


@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
			
		int studentId = Integer.parseInt(request.getParameter("studentId"));
		String studentName = request.getParameter("StudentName");
		String course = request.getParameter("course");
		String gender = request.getParameter("gender");
		String mobile = request.getParameter("mobile");
		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
			
Student student = new Student(studentId, studentName, course, gender,mobile, emailId, password);
		
		StudentDao studentDao = new StudentDao();
		int result = studentDao.studentRegister(student);
		
		out.print("<body bgcolor='lightyellow' text='green'>");
		out.print("<center>");
		if (result > 0) {
			out.print("<h1>Student Registration Success</h1>");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentLoginPage.html");
			requestDispatcher.include(request, response);
		} else {
			out.print("<h1 style='color:red'>Student Registration Failed...</h1>");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("StudentRegister.html");
			requestDispatcher.include(request, response);
		}

		out.print("</center></body>");

	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
