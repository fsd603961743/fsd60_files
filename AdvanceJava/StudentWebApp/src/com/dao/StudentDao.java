package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Student;

public class StudentDao {
	public Student studentLogin(String emailId, String password) {

		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String loginQuery = "Select * from student where emailId=? and password=?";

		try {
			preparedStatement = connection.prepareStatement(loginQuery);
			preparedStatement.setString(1, emailId);
			preparedStatement.setString(2, password);
			resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {

				Student student = new Student();

				student.setStudentId(resultSet.getInt(1));
				student.setStudentName(resultSet.getString(2));
				student.setCourse(resultSet.getString(3));
				student.setGender(resultSet.getString(4));
				student.setMobile(resultSet.getString(5));
				student.setEmailId(resultSet.getString(6));
				student.setPassword(resultSet.getString(7));

				return student;

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return null;
	}
	public int studentRegister(Student student) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;

		String registerQuery = "insert into student values (?,?,?,?,?,?,?)";

		try {
			preparedStatement = connection.prepareStatement(registerQuery);

			preparedStatement.setInt(1, student.getStudentId());
			preparedStatement.setString(2, student.getStudentName());
			preparedStatement.setString(3, student.getCourse());
			preparedStatement.setString(4, student.getGender());
			preparedStatement.setString(5, student.getMobile());
			preparedStatement.setString(6, student.getEmailId());
			preparedStatement.setString(7, student.getPassword());

			return preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {
				if (connection != null) {
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return 0;
	}
	public List<Student> getAllStudents() {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "Select * from student";
		List<Student> studentList = new ArrayList<Student>();
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			resultSet = preparedStatement.executeQuery();
			
			if (resultSet != null) {
				
				while (resultSet.next()) {
					
					Student student = new Student();
					
					student.setStudentId(resultSet.getInt(1));
					student.setStudentName(resultSet.getString(2));
					student.setCourse(resultSet.getString(3));
					student.setGender(resultSet.getString(4));
					student.setMobile(resultSet.getString(5));
					student.setEmailId(resultSet.getString(6));
					student.setPassword(resultSet.getString(7));
					
					studentList.add(student);
				}
				
				return studentList;
			} 
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		
		return null;
	}
	public Student getStudentById(int StudentId) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "Select * from student where studentId=?";
		
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			preparedStatement.setInt(1, StudentId);
			resultSet = preparedStatement.executeQuery();

			
			if (resultSet.next()) {
				
				
					
					Student student = new Student();
					
					student.setStudentId(resultSet.getInt(1));
					student.setStudentName(resultSet.getString(2));
					student.setCourse(resultSet.getString(3));
					student.setGender(resultSet.getString(4));
					student.setMobile(resultSet.getString(5));
					student.setEmailId(resultSet.getString(6));
					student.setPassword(resultSet.getString(7));
					
					return student;
				}
				
				
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
		
		return null;
		
	}
	public Student getStudentByName(String studentName) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		
		String selectQuery = "Select * from student where studentName=?";
		
		
		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			preparedStatement.setString(1, studentName);
			resultSet = preparedStatement.executeQuery();

			
			if (resultSet.next()) {
				
				
					
					Student student = new Student();
					
					student.setStudentId(resultSet.getInt(1));
					student.setStudentName(resultSet.getString(2));
					student.setCourse(resultSet.getString(3));
					student.setGender(resultSet.getString(4));
					student.setMobile(resultSet.getString(5));
					student.setEmailId(resultSet.getString(6));
					student.setPassword(resultSet.getString(7));
					
					return student;
				}
				
				
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {
				if (connection != null) {
					resultSet.close();
					preparedStatement.close();
					connection.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	



}
