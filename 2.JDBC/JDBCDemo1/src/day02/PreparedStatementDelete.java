package day02;

import java.sql.*;
import java.util.*;

import com.db.DbConnection;

public class PreparedStatementDelete {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Employee Details");
		
		int empId = scanner.nextInt();
		System.out.println();
		
		String insertQuery = "delete from employee where empid=?";
		
		try {
			preparedStatement = connection.prepareStatement(insertQuery);
						
			preparedStatement.setInt(1, empId);
			
			
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("record deleted");
			} else {
				System.out.println("Failed!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}


	}

}
