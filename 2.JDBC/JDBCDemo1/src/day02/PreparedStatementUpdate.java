package day02;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

public class PreparedStatementUpdate {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Employee Details");
		
		int empId = scanner.nextInt();
		
		double salary = scanner.nextDouble();
		
		System.out.println();
		
		String updateQuery = "update employee set salary = ? where empid =?";
		
		try {
			preparedStatement = connection.prepareStatement(updateQuery);
			
			preparedStatement.setDouble(1, salary);
			
			preparedStatement.setInt(2, empId);
		
			
			int result = preparedStatement.executeUpdate();
			
			if (result > 0) {
				System.out.println("Record updated into the table");
			} else {
				System.out.println("Record updation Failed!!!");
			}
			
			preparedStatement.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}


	}

}
