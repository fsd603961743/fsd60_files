package day02;

import java.sql.*;

import com.db.DbConnection;

public class PrintColumns {

	public static void main(String[] args) {
	        
		try {
			Connection connection = DbConnection.getConnection();
			
			Statement statement = connection.createStatement();
			
			String tableName = "employee";
			
			String query = "Select * from "+tableName;
			
			ResultSet resultSet = statement.executeQuery(query);
			
			ResultSetMetaData metaData = resultSet.getMetaData();
			
			System.out.println("Columns of table " + tableName + " :");
			
			int columnCount = metaData.getColumnCount();
			
			for (int i = 1; i <= columnCount; i++) {
				
			System.out.println("Column " + i + " : " + metaData.getColumnName(i));
				
			}
			
			connection.close();
			
		} catch (SQLException e) {
			
			e.printStackTrace();
			
		}

	}

}
