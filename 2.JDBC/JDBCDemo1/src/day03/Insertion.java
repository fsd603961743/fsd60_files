
package day03;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.db.DbConnection;

//Insert an Employee Record
public class Insertion {
	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;

		String selectQuery = "Select * from Employee";

		try {
			preparedStatement = connection.prepareStatement(selectQuery);
			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("");
				}
			} else {
				System.out.println("Unable to Fetch Employee Records");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("\n");
		
		//-------------------------------------------------------

		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter Employee Details");

		int empId = scanner.nextInt();
		String empName = scanner.next();
		double salary = scanner.nextDouble();
		String gender = scanner.next();
		String emailId = scanner.next();
		String password = scanner.next();
		System.out.println();

		String insertQuery = "Insert into employee values (?, ?, ?, ?, ?, ?)";

		try {
			connection = DbConnection.getConnection();
			preparedStatement = connection.prepareStatement(insertQuery);

			preparedStatement.setInt(1, empId);
			preparedStatement.setString(2, empName);
			preparedStatement.setDouble(3, salary);
			preparedStatement.setString(4, gender);
			preparedStatement.setString(5, emailId);
			preparedStatement.setString(6, password);

			int result = preparedStatement.executeUpdate();

			if (result > 0) {
				System.out.println("Record Inserted into the table");
			} else {
				System.out.println("Record Insertion Failed!!!");
			}

			preparedStatement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("\n");
		
		//---------------------------------------------------------------

		try {
			connection = DbConnection.getConnection();
			preparedStatement = connection.prepareStatement("Select * from employee");
			resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				while (resultSet.next()) {
					System.out.print(resultSet.getInt(1)    + " " + resultSet.getString(2) + " ");
					System.out.print(resultSet.getDouble(3) + " " + resultSet.getString(4) + " ");
					System.out.print(resultSet.getString(5) + " " + resultSet.getString(6) + " ");
					System.out.println("");
				}
			} else {
				System.out.println("Unable to Fetch Employee Records");
			}

			resultSet.close();
			preparedStatement.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			try {				
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}		
	}
}
