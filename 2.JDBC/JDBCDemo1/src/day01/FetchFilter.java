package day01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class FetchFilter {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Connection connection = DbConnection.getConnection();
        Statement statement = null;
        ResultSet resultSet = null;
        ResultSet resultSet1 = null;

        String selectQuery = "select * from employee";
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(selectQuery);
            while(resultSet.next()){
            	
                System.out.print(resultSet.getInt(1)+" ");

            }
            System.out.println();
            System.out.print("Enter the empid: ");
            int empId = sc.nextInt();
            System.out.println();
            String selectFilter = "select * from employee where empid =" + empId;

            resultSet1 = statement.executeQuery(selectFilter);
            

            if (resultSet1.next()) {
                System.out.println("Empid   : " + resultSet1.getInt(1));
                System.out.println("EmpName : " + resultSet1.getString(2));
                System.out.println("Salary  : " + resultSet1.getInt(3));
                System.out.println("Gender  : " + resultSet1.getString(4));
                System.out.println("EmailId : " + resultSet1.getString(5));
                System.out.println("Password: " + resultSet1.getString(6));
                System.out.println();
            } else {
                System.out.println("No record found with id: " + empId);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
