package day01;

import java.sql.*;

import java.util.Scanner;

import com.db.DbConnection;

public class InsertProgram {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		System.out.println("Enter Employee id");
		int empId = sc.nextInt();
		System.out.println("Enter employee Name");
		String empName = sc.next();
		System.out.println("Enter salary");
		double salary = sc.nextDouble();
		System.out.println("Enter gender");
		String gender = sc.next();
		System.out.println("Enter mail id");
		String mail = sc.next();
		System.out.println("Enter password");
		String password = sc.next();
		
		String insertQuery = "insert into employee values ("+empId+", '"+empName+"', " 
				+salary+", '"+gender+"', '"+mail+"', '"+password+"')";
		try{
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);
			if (result > 0) {
				System.out.println(result + " Record(s) Inserted...");
			} else {
				System.out.println("Record Insertion Failed...");
			}
		}
		catch(Exception e){
			e.printStackTrace();
			
		}
		try {
			if (connection != null) {
				statement.close();
				connection.close();
				sc.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
