package day01;

import java.sql.*;

import com.db.DbConnection;

public class Demo5 {

	public static void main(String[] args) {
		Connection connection  = DbConnection.getConnection();
		Statement statement = null;
		ResultSet  resultSet= null;
		String selectQuery = "select * from employee";
		try{
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);
			while(resultSet.next()){
			System.out.println("Empid	: "+resultSet.getInt(1));
			System.out.println("EmpName : "+resultSet.getString(2));
			System.out.println("Salary	: "+resultSet.getInt(3));
			System.out.println("Gender	: "+resultSet.getString(4));
			System.out.println("EmailId : "+resultSet.getString(5));
			System.out.println("Password: "+resultSet.getString(6));
			System.out.println();
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}

	}

}
