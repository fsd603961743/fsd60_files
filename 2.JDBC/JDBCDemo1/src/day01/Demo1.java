package day01;

import java.sql.Connection;
import java.sql.DriverManager;

public class Demo1 {

	public static void main(String[] args) {
		Connection con = null;
		String url = "jdbc:mysql://sql302.byethost33.com:3306/b33_35929335_fsd60";
		String userName = "b33_35929335";
		String password = "Naveen426";
		try {

			// 1. Loading the MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");

			// 2. Establishing the Connection
			con = DriverManager.getConnection(url, userName, password);

			if (con != null) {
				System.out.println("Successfully Established the Connection");

				con.close();

			} else {
				System.out.println("Failed to Establish the Connection");
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Failed to Establish the Connection");
		}

	}
}
