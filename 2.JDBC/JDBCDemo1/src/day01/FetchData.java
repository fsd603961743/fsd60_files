package day01;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

import com.db.DbConnection;

public class FetchData {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		ResultSet resultSet = null;
		System.out.println("Enter the empid");
		int empId = sc.nextInt();
		String selectQuery = "select * from employee where empid =" + empId;
		try {
			statement = connection.createStatement();
			resultSet = statement.executeQuery(selectQuery);
			// System.out.println(resultSet.equals(null));
			if (resultSet.next()) {
				System.out.println("Empid	: " + resultSet.getInt(1));
				System.out.println("EmpName : " + resultSet.getString(2));
				System.out.println("Salary	: " + resultSet.getInt(3));
				System.out.println("Gender	: " + resultSet.getString(4));
				System.out.println("EmailId : " + resultSet.getString(5));
				System.out.println("Password: " + resultSet.getString(6));
				System.out.println();
			} else {
				System.out.println("No record found with id: " + empId);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
