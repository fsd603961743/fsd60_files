package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Demo4 {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		int empid=109;
		double newSalary = 25000;
		String deleteQuery = "delete from employee where empid="+ empid;
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(deleteQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) deleted...");
			} else {
				System.out.println("Record deletion Failed...");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

}
