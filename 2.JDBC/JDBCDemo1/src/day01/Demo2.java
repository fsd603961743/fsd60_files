package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Demo2 {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		
		int empId = 108;
		String empName = "robin";
		double salary = 90098.98;
		String gender = "Male";
		String emailId = "robin@gmail.com";
		String password = "1234";
		
		String insertQuery = "insert into employee values (" + 
				empId + ", '" + empName + "', " + salary + ", '" + 
				gender + "', '" + emailId + "', '" + password + "')";
				
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(insertQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Inserted...");
			} else {
				System.out.println("Record Insertion Failed...");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	

	}

}
