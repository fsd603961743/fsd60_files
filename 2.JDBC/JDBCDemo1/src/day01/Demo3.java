package day01;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.db.DbConnection;

public class Demo3 {

	public static void main(String[] args) {
		Connection connection = DbConnection.getConnection();
		Statement statement = null;
		int empid=107;
		double newSalary = 25000;
		String updateQuery = "update employee set salary ="+newSalary+"where empid="+ empid;
		try {
			statement = connection.createStatement();
			int result = statement.executeUpdate(updateQuery);
			
			if (result > 0) {
				System.out.println(result + " Record(s) Updated...");
			} else {
				System.out.println("Record Updation Failed...");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			if (connection != null) {
				statement.close();
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
